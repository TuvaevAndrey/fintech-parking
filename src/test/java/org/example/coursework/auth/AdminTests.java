package org.example.coursework.auth;

import org.example.coursework.AbstractTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithAnonymousUser;

@SpringBootTest
@WithAnonymousUser
public class AdminTests extends AbstractTest {

}
